TARGET = gol
CC     = gcc
CFLAGS = `pkg-config --cflags sdl glew`
LIBS   = `pkg-config --libs sdl gl glu glew`
SRCS   = gol.c file.c

all: $(TARGET)
	
$(TARGET): $(SRCS:.c=.o)
	$(CC) $(SRCS:.c=.o) -o $(TARGET) $(LIBS)

%.o: %.d

%.d: %.c
	$(CC) -M $(CFLAGS) -o $*.d $<

clean:
	$(RM) $(SRCS:.c=.o) $(SRCS:.c=.d)

include $(SRCS:.c=.d)

