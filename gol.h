#ifndef _GOL_H
#define _GOL_H

#ifndef FALSE
#define FALSE (0)
#endif
#ifndef TRUE
#define TRUE (!FALSE)
#endif

#define GL_CHECK_ERROR \
	if ((gl_error = glGetError()) != GL_NO_ERROR) fprintf(stderr, "%s:%d: %s\n", __FILE__, __LINE__, gluErrorString(gl_error));

#endif

