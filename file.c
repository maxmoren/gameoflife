#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>

struct file
{
	void *base;
	size_t len;
	int fd;
};

struct file *file_load(const char *path, size_t len)
{
	struct stat buf;
	struct file *desc;

	if ((desc = malloc(sizeof(desc))) == NULL)
	{
		perror("malloc");
		return NULL;
	}

	if ((desc->fd = open(path, O_RDONLY)) < 0)
	{
		perror("open");
		return NULL;
	}

	if (fstat(desc->fd, &buf))
	{
		close(desc->fd);
		perror("fstat");
		return NULL;
	}

	if (len == 0)
		desc->len = buf.st_size;
	else
	{
		if (len > buf.st_size)
		{
			close(desc->fd);
			free(desc);

			fprintf(stderr, "requested to map %lu bytes but file is only %lu bytes\n", len, buf.st_size);

			return NULL;
		}

		desc->len = len;
	}

	if ((desc->base = mmap(NULL, desc->len, PROT_READ, MAP_PRIVATE, desc->fd, 0)) == NULL)
	{
		close(desc->fd);
		free(desc);
		perror("mmap");
		return NULL;
	}

	return desc;
}

void *file_ptr(struct file *desc)
{
	return desc->base;
}

size_t file_size(struct file *desc)
{
	return desc->len;
}

void file_close(struct file *desc)
{
	munmap(desc->base, desc->len);
	close(desc->fd);
	free(desc);
}

