#ifndef _FILE_H
#define _FILE_H

#define __need_size_t
#include <stddef.h>

struct file;
struct file *file_load(const char *, size_t len);
void *file_ptr(struct file *);
size_t file_size(struct file *);
void file_close(struct file *);

#endif

