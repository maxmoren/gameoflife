#include <assert.h>

#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL/SDL.h>

#include "gol.h"
#include "file.h"

#define SCREEN_W (512)
#define SCREEN_H (512)

static int tick = 0;

static GLuint fbo;
static GLuint tex0, tex1;

static GLint loc_tex = -1;
static GLint loc_tick = -1;

static GLuint color_program = 0;

enum
{
	EV_STEP = SDL_USEREVENT,
	EV_FPS
};

GLenum gl_error = GL_NO_ERROR;

static SDL_Surface *screen;

static GLuint load_shader(const char *name, const GLchar *src, GLint src_len, GLuint type)
{
	GLuint shader = glCreateShader(type);

	glShaderSource(shader, 1, &src, &src_len);
	glCompileShader(shader);

	GLint status = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if (status != GL_TRUE)
	{
		char message[1024];

		memset(message, '\0', 1024);

		glGetShaderInfoLog(shader, 1024, NULL, message);
		glDeleteShader(shader);

		fprintf(stderr, "%s: Compiler %s", name, message);
		return 0;
	}

	return shader;
}

GLuint shadowMapUniform = 0;

static void load_program(const char *vertex_path, const char *fragment_path, GLuint *target)
{
	GLuint program;
	struct file *vertex_file, *fragment_file;

	if ((vertex_file = file_load(vertex_path, 0)) == NULL ||
	    (fragment_file = file_load(fragment_path, 0)) == NULL)
		exit(EXIT_FAILURE);

	GLuint vshader = load_shader(vertex_path, file_ptr(vertex_file), file_size(vertex_file), GL_VERTEX_SHADER);
	GLuint fshader = load_shader(fragment_path, file_ptr(fragment_file), file_size(fragment_file), GL_FRAGMENT_SHADER);

	file_close(vertex_file);
	file_close(fragment_file);

	if (vshader == 0 || fshader == 0)
		return;

	if ((program = glCreateProgram()) == 0)
	{
		fprintf(stderr, "glCreateProgram: %s\n", gluErrorString(glGetError()));

		if (color_program == 0)
			exit(EXIT_FAILURE);
		else
			return;
	}

	glAttachShader(program, vshader);
	glAttachShader(program, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);

	glLinkProgram(program);

	GLint status = GL_FALSE;
	glGetProgramiv(program, GL_LINK_STATUS, &status);

	if (status != GL_TRUE)
	{
		char message[1024];
		GLsizei length = 0;

		glGetProgramInfoLog(program, 1024, &length, message);
		glDeleteProgram(program);

		fprintf(stderr, "Linker %s", message);
		return;
	}

	shadowMapUniform = glGetUniformLocationARB(program, "ShadowMap");

	if (*target != 0)
		glDeleteProgram(*target);

	*target = program;

	printf("Loaded program \"%s\", \"%s\".\n", vertex_path, fragment_path);
}

static Uint32 fps_cb(Uint32 interval, void *param)
{
	SDL_Event event;

	event.type = SDL_USEREVENT;
	event.user.code = EV_FPS;
	event.user.data1 = NULL;
	event.user.data2 = NULL;

	SDL_PushEvent(&event);

	return interval;
}

static void reload_shaders(void)
{
	load_program("color.vertex.glsl", "color.fragment.glsl", &color_program);

	if (color_program != 0)
	{
		if ((loc_tex = glGetUniformLocation(color_program, "texture")) == -1)
			fprintf(stderr, "Could not get location of uniform \"texture\".\n");

		if ((loc_tick = glGetUniformLocation(color_program, "tick")) == -1)
			fprintf(stderr, "Could not get location of uniform \"tick\".\n");
	}
}

void init_graphics(void)
{
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
	{
		fprintf(stderr, "error: SDL_Init: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	atexit(SDL_Quit);

	SDL_GL_SetAttribute(SDL_GL_RED_SIZE,     8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,   8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,    8);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,   8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,   0);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, TRUE);
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, FALSE);

	if (!SDL_VideoModeOK(SCREEN_W, SCREEN_H, 32, SDL_OPENGL))
	{
		fprintf(stderr, "error: Mode %dx%d not available.\n", SCREEN_W, SCREEN_H);
		exit(EXIT_FAILURE);
	}

	if ((screen = SDL_SetVideoMode(SCREEN_W, SCREEN_H, 32, SDL_OPENGL)) == NULL)
	{
		fprintf(stderr, "error: SDL_SetVideoMode: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	GLenum err = glewInit();

	if (GLEW_OK != err)
	{
		fprintf(stderr, "error: %s\n", glewGetErrorString(err));
		exit(EXIT_FAILURE);
	}

	/* Setup the view. */
	glMatrixMode(GL_PROJECTION);
	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

	glEnable(GL_TEXTURE_RECTANGLE_ARB);

	/* Reload shaders for the first time. */
	reload_shaders();
}

void shutdown_graphics(void)
{
}

GLuint create_texture(unsigned char *data)
{
	GLuint texture;

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	GL_CHECK_ERROR

    /* Create a new empty texture. 8-bit single channel. */
    glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, SCREEN_W, SCREEN_H, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);

	return texture;
}

int init_fbo(const char *path)
{
	struct file *f = file_load(path, SCREEN_W * SCREEN_H * 4);
	assert(f != NULL);
	void *ptr = file_ptr(f);

	/* Create a texture for color data. */
	tex0 = create_texture(ptr);
	tex1 = create_texture(NULL);

	file_close(f);

	/* Create a FBO. */
  	glGenFramebuffersEXT(1, &fbo);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

	/* Attach the textures to the first two color attachment points of FBO. */
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE, tex0, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT1_EXT, GL_TEXTURE_RECTANGLE, tex1, 0);

	GLenum err = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);

	if (err != GL_FRAMEBUFFER_COMPLETE_EXT)
	{
		fprintf(stderr, "glCheckFrameBufferStatusEXT: %s\n", gluErrorString(glGetError()));
		return 0;
	}

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	return 1;
}

static void draw_quad(void)
{
	glBegin(GL_QUADS);
		glTexCoord2i(0, 0); glVertex2i(-1, -1);
		glTexCoord2i(SCREEN_W, 0); glVertex2i( 1, -1);
		glTexCoord2i(SCREEN_W, SCREEN_H); glVertex2i( 1,  1);
		glTexCoord2i(0, SCREEN_H); glVertex2i(-1,  1);
	glEnd();
}

void render_screen()
{
	/* Turn off shaders. We want to use the fixed pipeline. */
	glUseProgram(0);

	/* Render on screen. */
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	if (tick == 0)
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex0);
	else
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex1);


	glDrawBuffer(GL_BACK);

	draw_quad();
}

void render(void)
{
	tick = !tick;

	/* Render off screen. */
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);

	/* Enable the shader program. */
	glUseProgram(color_program);

	if (loc_tick != -1) glUniform1i(loc_tick, tick);

	if (tick == 0)
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT0_EXT);
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex1);
	}
	else
	{
		glDrawBuffer(GL_COLOR_ATTACHMENT1_EXT);
		glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex0);
	}

	// glClear(GL_COLOR_BUFFER_BIT);

	draw_quad();
}

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		fprintf(stderr, "usage: gol PATH\n");
		return EXIT_FAILURE;
	}

	init_graphics();

	if (init_fbo(argv[1]) == 0)
		return 0;

	SDL_TimerID fps_timer;

	int frames = 0;
	// char title[] = "Game of Life (  0 FPS)";

	if ((fps_timer = SDL_AddTimer(999, fps_cb, NULL)) == NULL)
	{
		fprintf(stderr, "error: SDL_AddTimer: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	SDL_Event event;

	for (;;)
	{
		while (SDL_PollEvent(&event) == 1)
		{
			switch (event.type)
			{
				case SDL_USEREVENT:
					if(event.user.code == EV_FPS)
					{
						// snprintf(title, sizeof(title), "Game of Life (%3d FPS)", frames);
						// SDL_WM_SetCaption(title, "Game of Life");
						printf("fps: %d\n", frames);
						frames = 0;
					}
					break;

				case SDL_KEYDOWN:
					if (event.key.keysym.sym == 'r')
					{
						reload_shaders();
						break;
					}
					else if (event.key.keysym.sym == SDLK_RETURN)
					{
						render_screen();
						SDL_GL_SwapBuffers();
						break;
					}
					else if (event.key.keysym.sym != SDLK_ESCAPE)
						break;

				case SDL_QUIT:
					exit(EXIT_SUCCESS);
					break;
			}
		}

		render();
		render_screen();
		SDL_GL_SwapBuffers();
		++frames;
	}

	shutdown_graphics();

	return EXIT_SUCCESS;
}

