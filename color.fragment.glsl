#ifdef NVIDIA
#version 140
#extension GL_ARB_texture_rectangle: enable
#else
#version 120
#endif

uniform sampler2DRect texture;
uniform int tick;

bool alive(vec2 coord)
{
	int nw = int(texture2DRect(texture, coord + vec2(-1.0, -1.0)).r > 0.5);
	int n  = int(texture2DRect(texture, coord + vec2( 0.0, -1.0)).r > 0.5);
	int ne = int(texture2DRect(texture, coord + vec2(+1.0, -1.0)).r > 0.5);
	int w  = int(texture2DRect(texture, coord + vec2(-1.0,  0.0)).r > 0.5);
	int c  = int(texture2DRect(texture, coord + vec2( 0.0,  0.0)).r > 0.5);
	int e  = int(texture2DRect(texture, coord + vec2(+1.0,  0.0)).r > 0.5);
	int sw = int(texture2DRect(texture, coord + vec2(-1.0, +1.0)).r > 0.5);
	int s  = int(texture2DRect(texture, coord + vec2( 0.0, +1.0)).r > 0.5);
	int se = int(texture2DRect(texture, coord + vec2(+1.0, +1.0)).r > 0.5);

	int sum = nw + n + ne + w + e + sw + s + se;

	if (c == 1)
	{
		if (sum < 2)
			return false;

		if (sum > 3)
			return false;

		return true;
	}
	else
	{
		if (sum == 3)
			return true;

		return false;
	}
}

void main(void)
{
	vec2 coord = gl_FragCoord.xy;

	gl_FragData[0] = vec4(float(alive(coord)));
}
